

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>

.. un·e
.. ❤️💛💚

.. https://framapiaf.org/web/tags/ldh.rss

.. _ldh_grenoble:

===============================================================================
**La Ligue des droits de l'Homme (LDH) Grenoble et métropole** 
===============================================================================

- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm
- https://www.ldh-france.org/sujet/racisme-antisemitisme/
- https://piaille.fr/@LDH_Fr
- https://www.facebook.com/LDH.Grenoble/
- https://www.ldh-france.org/wp-content/uploads/2021/03/Guide-Lutter-contre-le-racisme-1.pdf

ligue-des-droits-de-l-homme-et-du-citoyen
================================================

Nom du contact : Pierre GAILLARD
E-mail : ldh-grenoble@orange.fr

::

	Adresse : Maison des Associations
	6, rue Berthe de Boissieux
	38000 Grenoble
	Boîte aux lettres n°88

Téléphone : 04 76 56 90 44 / 06 77 83 37 26
Informations complémentaires : Permanences : jeudi 17h-19h et sur RDV
Site internet : http://www.ldh-france.org
Nombre d'adhérents grenoblois : 110

::
    
    https://fr.wikipedia.org/wiki/Ligue_des_droits_de_l%27homme_(France)
    

- https://rstockm.github.io/mastowall/?hashtags=droitshumains,ldh,cncdh&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=ldh&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=antisemitisme&server=https://framapiaf.org
